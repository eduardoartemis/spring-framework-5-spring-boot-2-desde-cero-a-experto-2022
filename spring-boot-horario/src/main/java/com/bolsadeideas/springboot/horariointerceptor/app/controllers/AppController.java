/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bolsadeideas.springboot.horariointerceptor.app.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Eduardo
 */
@Controller
public class AppController {

    @Value("${config.horario.apertura}")
    private Integer apertura;
    @Value("${config.horario.cierre}")
    private Integer cierre;

    @GetMapping({"/", "/index"})
    public String index(Model model) {
        model.addAttribute("titulo", "Bienvenido al Horario de Atención a Clientes");
        return "index";
    }

    @GetMapping("/cerrado")
    public String cerrado(Model model) {
        StringBuilder mensaje = new StringBuilder("Cerrado, por favor visitenos desde las ");
        mensaje.append(apertura);
        mensaje.append(" y las ");
        mensaje.append(cierre);
        mensaje.append(" hrs. Gracias.");
        model.addAttribute("titulo", "Fuera de Horario de Atención");
        model.addAttribute("mensaje", mensaje.toString());
        return "cerrado";
    }

}
