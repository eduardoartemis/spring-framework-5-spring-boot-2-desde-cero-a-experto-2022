package com.bolsadeideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Eduardo Vicente
 */
@Controller
public class HomeController {

    @GetMapping("/")
    public String home() {
        //return "redirect:/app/index";
        return "forward:/app/index";
    }
}
