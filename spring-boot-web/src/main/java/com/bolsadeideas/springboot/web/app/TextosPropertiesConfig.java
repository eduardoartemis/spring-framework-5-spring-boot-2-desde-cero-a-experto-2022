package com.bolsadeideas.springboot.web.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 *
 * @author Eduardo Vicente
 */
@Configuration
@PropertySources({
    @PropertySource("classpath:texto.properties")
})
public class TextosPropertiesConfig {

}
