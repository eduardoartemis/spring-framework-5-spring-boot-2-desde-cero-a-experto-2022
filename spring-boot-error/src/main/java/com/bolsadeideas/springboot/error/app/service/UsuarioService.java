/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bolsadeideas.springboot.error.app.service;

import com.bolsadeideas.springboot.error.app.models.domain.Usuario;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Eduardo
 */
public interface UsuarioService {

    public List<Usuario> listar();

    public Usuario obtenerPorId(Integer id);

    public Optional<Usuario> obtenerPorIdOptional(Integer id);

}
