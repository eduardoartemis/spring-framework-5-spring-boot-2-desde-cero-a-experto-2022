/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bolsadeideas.springboot.error.app.service;

import com.bolsadeideas.springboot.error.app.models.domain.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Eduardo
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    private List<Usuario> lista;

    public UsuarioServiceImpl() {
        this.lista = new ArrayList<>();
        this.lista.add(new Usuario(1, "Andres", "Guzman"));
        this.lista.add(new Usuario(2, "Eduardo", "Vicente"));
        this.lista.add(new Usuario(3, "Maria", "Vicente"));
        this.lista.add(new Usuario(4, "Juan", "Vicente"));
        this.lista.add(new Usuario(5, "Pedro", "Vicente"));
        this.lista.add(new Usuario(6, "Roxana", "Vicente"));
        this.lista.add(new Usuario(7, "Diego", "Vicente"));
    }

    @Override
    public List<Usuario> listar() {
        return this.lista;
    }

    @Override
    public Usuario obtenerPorId(Integer id) {
        Usuario resultado = null;
        for (Usuario u : this.lista) {
            if (u.getId().equals(id)) {
                resultado = u;
                break;
            }
        }

        return resultado;
    }

    @Override
    public Optional<Usuario> obtenerPorIdOptional(Integer id) {
        Usuario usuario = this.obtenerPorId(id);
        return Optional.ofNullable(usuario);
    }

}
