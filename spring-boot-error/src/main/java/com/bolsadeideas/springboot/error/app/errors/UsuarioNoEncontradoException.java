/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bolsadeideas.springboot.error.app.errors;

/**
 *
 * @author Eduardo
 */
public class UsuarioNoEncontradoException extends RuntimeException {

    public UsuarioNoEncontradoException(String id) {
        super("Usuario con ID: ".concat(id).concat(" no existe en el sistema"));
    }

    private static final long serialVersionUID = 1L;

}
