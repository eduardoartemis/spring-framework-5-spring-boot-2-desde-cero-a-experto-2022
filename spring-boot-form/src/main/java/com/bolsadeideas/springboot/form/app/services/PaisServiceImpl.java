/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bolsadeideas.springboot.form.app.services;

import com.bolsadeideas.springboot.form.app.models.domain.Pais;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Eduardo
 */
@Service
public class PaisServiceImpl implements PaisService {

    private List<Pais> lista;

    public PaisServiceImpl() {
        this.lista = Arrays.asList(
                new Pais(1, "ES", "España"),
                new Pais(2, "ME", "Mexico"),
                new Pais(3, "CL", "Chile"),
                new Pais(4, "AR", "Argentina"),
                new Pais(5, "PE", "Perú"),
                new Pais(6, "CO", "Colombia"),
                new Pais(7, "VE", "Venezuela"));
    }

    @Override
    public List<Pais> listar() {
        return lista;
    }

    @Override
    public Pais obtenerPorId(Integer id) {
        Pais resultado = null;

        for (Pais pais : this.lista) {
            if (id == pais.getId()) {
                resultado = pais;
                break;
            }
        }

        return resultado;

    }

}
