/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.bolsadeideas.springboot.form.app.services;

import com.bolsadeideas.springboot.form.app.models.domain.Role;
import java.util.List;

/**
 *
 * @author Eduardo
 */
public interface RoleService {

    public List<Role> listar();

    public Role obtenerPorId(Integer id);
}
