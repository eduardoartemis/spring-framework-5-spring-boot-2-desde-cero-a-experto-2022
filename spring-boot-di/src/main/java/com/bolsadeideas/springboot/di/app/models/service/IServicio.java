package com.bolsadeideas.springboot.di.app.models.service;

/**
 *
 * @author Eduardo Vicente
 */
public interface IServicio {

    public String operacion();

}
