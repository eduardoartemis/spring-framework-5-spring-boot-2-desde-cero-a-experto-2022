package com.bolsadeideas.springboot.di.app.models.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 *
 * @author Eduardo Vicente
 */
//@Component("miServicioPrincipal")
//@Primary
public class MiServicio implements IServicio {

    @Override
    public String operacion() {
        return "ejecuntando algun proceso importante simple";
    }
}
